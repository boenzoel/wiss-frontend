import React from "react";
import ApexCharts from "react-apexcharts";
import { useTheme } from "@material-ui/styles";
import axios from 'axios';

var series = [
    {
        name: 'Total Anggota',
        data: []
    }, 
];

var categories = [];

const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042', '#ff94f6', '#cbff94'];

export default function ApexColumnChartYearly() {
    var theme = useTheme();

    var url = 'http://localhost:8080/dashboard?target=anggota_register_yearly_summary';

    axios({
        method: 'GET',
        url: url,
    }).then(function(response,index) {
        // chart.updateSeries([{
        //     name: 'Data Pinjaman',
        //     data: response.data
        // }])
        let result = response.data.data;
        let i = 0;
        result.forEach(element => {
            console.log('dd',element)
            categories.push(element.Tahun)
            series[0].data[i] = element.Value
            i++;
        });
    })

    return (
        <ApexCharts
        options={themeOptions(theme,categories,COLORS)}
        series={series}
        type="bar"
        height={350}
        colors={COLORS}
        />
    );
}

// ############################################################
function themeOptions(theme, categories, colors) {
    return {
        plotOptions: {
            bar: {
                horizontal: false,
                columnWidth: '25%',
                endingShape: 'rounded',
                distributed: true,
            },
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            show: true,
            width: 2,
            colors: ['transparent']
        },
        xaxis: {
            categories: categories,
        },
        yaxis: {
            title: {
                text: 'Jumlah Anggota : '
            }
        },
        fill: {
            opacity: 1,
            // colors: ['#FF0000', '#FF00AB', '#00B500'],
            // colors: [function({ value, seriesIndex, w }) {
            //     if(colors.length > seriesIndex) {
            //         return colors[0];
            //     }
            //     return colors[seriesIndex];
            // }]
        },
        tooltip: {
            y: {
                formatter: function (val) {
                return "Jumlah Anggota : " + val 
                }
            }
        }
    };
}
