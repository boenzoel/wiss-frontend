
import React, { Component }  from 'react';
import FusionCharts from "fusioncharts";
import charts from "fusioncharts/fusioncharts.charts";
import ReactFusioncharts from "react-fusioncharts";
import axios from 'axios';

// Resolves charts dependancy
charts(FusionCharts);

var indexMonth = ['January','February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October','November','December'];
var categories = ['Jan','Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Oct','Nov','Des'];

var dataSource = {
  chart: {
    caption: "Data Anggota Tahunan",
    subcaption: new Date().getFullYear(),
    xaxisname: "Years",
    yaxisname: "Total number anggota register",
    formatnumberscale: "1",
    plottooltext:
      "<b>$dataValue</b> Total <b>$seriesName</b> in $label",
    theme: "candy"
  },
  categories: [
    {
      category: []
    }
  ],
  dataset: [
    {
      seriesname: "Anggota Registrasi",
      data: [
        {
          value: "0"
        },
        {
          value: "0"
        },
        {
          value: "0"
        },
        {
          value: "0"
        },
        {
          value: "0"
        },
        {
          value: "0"
        },
        {
          value: "0"
        },
        {
          value: "0"
        },
        {
          value: "0"
        },
        {
          value: "0"
        },
        {
          value: "0"
        },
        {
          value: "0"
        }
      ]
    },
  ]
};

export default function FusionBarChartYearly() {
    var url = 'http://localhost:8080/dashboard?target=anggota_register_yearly_summary';

    axios({
        method: 'GET',
        url: url,
    }).then(function(response,index) {
        let result = response.data.data;
        let i = 0;
        result.forEach(element => {
            dataSource.categories[0].category.push({'label': element.Tahun})
            dataSource.dataset[0].data[i].value = element.Value;
            i++;
        });

        console.log(dataSource.dataset[0])
    })

    return (
      <ReactFusioncharts
        type="mscolumn3d"
        width="100%"
        height="600"
        dataFormat="JSON"
        dataSource={dataSource}
      />
    );
}
