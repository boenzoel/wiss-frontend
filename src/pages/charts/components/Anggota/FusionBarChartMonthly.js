import React, { Component }  from 'react';
import FusionCharts from "fusioncharts";
import charts from "fusioncharts/fusioncharts.charts";
import ReactFusioncharts from "react-fusioncharts";
import axios from 'axios';

// Resolves charts dependancy
charts(FusionCharts);

var indexMonth = ['January','February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October','November','December'];
var categories = ['Jan','Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Oct','Nov','Des'];

var dataSource = {
  chart: {
    caption: "Data Anggota Bulanan",
    subcaption: new Date().getFullYear(),
    xaxisname: "Months",
    yaxisname: "Total number anggota register",
    formatnumberscale: "1",
    plottooltext:
      "<b>$dataValue</b> Total <b>$seriesName</b> in $label",
    theme: "candy"
  },
  categories: [
    {
      category: [
        {
          label: "January"
        },
        {
          label: "February"
        },
        {
          label: "March"
        },
        {
          label: "April"
        },
        {
          label: "May"
        },
        {
          label: "June"
        },
        {
          label: "July"
        },
        {
          label: "August"
        },
        {
          label: "September"
        },
        {
          label: "Oktober"
        },
        {
          label: "November"
        },
        {
          label: "December"
        }
      ]
    }
  ],
  dataset: [
    {
      seriesname: "Anggota Registrasi",
      data: [
        {
          value: "0"
        },
        {
          value: "0"
        },
        {
          value: "0"
        },
        {
          value: "0"
        },
        {
          value: "0"
        },
        {
          value: "0"
        },
        {
          value: "0"
        },
        {
          value: "0"
        },
        {
          value: "0"
        },
        {
          value: "0"
        },
        {
          value: "0"
        },
        {
          value: "0"
        }
      ]
    },
  ]
};

export default function FusionBarChartMonthly() {
    var url = 'http://localhost:8080/dashboard?target=anggota_register_monthly_summary';

    axios({
        method: 'GET',
        url: url,
    }).then(function(response,index) {
        let result = response.data.data;
        
        result.forEach(element => {
            var bulan = element.Periode;
            // console.log('dd',series[0].data)
            dataSource.dataset[0].data[indexMonth.indexOf(bulan) + 1].value = element.Value;
            console.log(bulan,(indexMonth.indexOf(bulan)+1) + ' = ' + element.Value)
        });

        console.log(dataSource.dataset[0])
    })

    return (
      <ReactFusioncharts
        type="mscolumn3d"
        width="100%"
        height="600"
        dataFormat="JSON"
        dataSource={dataSource}
      />
    );
}
