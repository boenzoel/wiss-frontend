import React, { Component }  from 'react';
import FusionCharts from "fusioncharts";
import charts from "fusioncharts/fusioncharts.charts";
import ReactFusioncharts from "react-fusioncharts";
import axios from 'axios';

// Resolves charts dependancy
charts(FusionCharts);

var dataSource = {
  chart: {
    caption: "",
    subcaption: "",
    showvalues: "1",
    showpercentintooltip: "1",
    numberprefix: "Total : ",
    enablemultislicing: "1",
    theme: "candy"
  },
  data: []
};

export default function FusionPieChart() {

    var url = 'http://localhost:8080/dashboard?target=anggota';

    axios({
        method: 'GET',
        url: url,
    }).then(function(response,index) {
        let result = response.data;
        dataSource.data = [];
        result.forEach(element => {
            dataSource.data.push( {
                'label' : element.name,
                'value' : element.value
            } );
        });
        console.log(result)
    })

    return (
      <ReactFusioncharts
        type="pie3d"
        width="100%"
        height="400"
        dataFormat="JSON"
        dataSource={dataSource}
      />
    );

}