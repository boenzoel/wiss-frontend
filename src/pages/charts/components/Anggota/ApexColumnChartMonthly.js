import React from "react";
import ApexCharts from "react-apexcharts";
import { useTheme } from "@material-ui/styles";
import axios from 'axios';

var series = [
    {
        name: 'Total Anggota Registrasi Bulanan',
        data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    }, 
];

var indexMonth = ['January','February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October','November','December'];
var categories = ['Jan','Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Oct','Nov','Des'];

const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042', '#ff94f6', '#cbff94'];

export default function ApexColumnChartMonthly() {
    var theme = useTheme();

    var url = 'http://localhost:8080/dashboard?target=anggota_register_monthly_summary';

    axios({
        method: 'GET',
        url: url,
    }).then(function(response,index) {
        let result = response.data.data;

        result.forEach(element => {
            var bulan = element.Periode;
            // console.log('dd',series[0].data)
            series[0].data[indexMonth.indexOf(bulan)] = element.Value

            // console.log(bulan,indexMonth.indexOf(bulan))
        });
    })

    return (
        <ApexCharts
        options={themeOptions(theme,categories,COLORS)}
        series={series}
        type="bar"
        height={350}
        colors={COLORS}
        />
    );
}

// ############################################################
function themeOptions(theme, categories, colors) {
    return {
        plotOptions: {
            bar: {
                horizontal: false,
                columnWidth: '55%',
                endingShape: 'rounded',
                distributed: true,
            },
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            show: true,
            width: 2,
            colors: ['transparent']
        },
        xaxis: {
            categories: categories,
        },
        yaxis: {
            title: {
                text: 'Total : '
            }
        },
        fill: {
            opacity: 1,
            // colors: ['#FF0000', '#FF00AB', '#00B500'],
            // colors: [function({ value, seriesIndex, w }) {
            //     if(colors.length > seriesIndex) {
            //         return colors[0];
            //     }
            //     return colors[seriesIndex];
            // }]
        },
        tooltip: {
            y: {
                formatter: function (val) {
                return "Total : " + val 
                }
            }
        }
    };
}
