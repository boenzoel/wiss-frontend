import React from "react";
import ApexCharts from "react-apexcharts";
import { useTheme } from "@material-ui/styles";
import axios from 'axios';

var series = [
    {
        name: 'Total Pinjaman',
        data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    }, 
];

var categories = ['Jan','Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Oct','Nov','Des'];

const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042', '#ff94f6', '#cbff94'];

export default function ApexColumnChartMonthly() {
    var theme = useTheme();

    var url = 'http://localhost:8080/dashboard?target=pinjaman_monthly_summary';

    axios({
        method: 'GET',
        url: url,
    }).then(function(response,index) {
        // chart.updateSeries([{
        //     name: 'Data Pinjaman',
        //     data: response.data
        // }])
        let result = response.data;

        result.forEach(element => {
            var bulan = element.bulan;
            // console.log('dd',series[0].data)
            series[0].data[bulan] = element.totalpinjaman
        });
    })

    return (
        <ApexCharts
        options={themeOptions(theme,categories,COLORS)}
        series={series}
        type="bar"
        height={350}
        colors={COLORS}
        />
    );
}

// ############################################################
function themeOptions(theme, categories, colors) {
    return {
        plotOptions: {
            bar: {
                horizontal: false,
                columnWidth: '55%',
                endingShape: 'rounded',
                distributed: true,
            },
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            show: true,
            width: 2,
            colors: ['transparent']
        },
        xaxis: {
            categories: categories,
        },
        yaxis: {
            title: {
                text: 'Rp. (Rupiah)'
            }
        },
        fill: {
            opacity: 1,
            // colors: ['#FF0000', '#FF00AB', '#00B500'],
            // colors: [function({ value, seriesIndex, w }) {
            //     if(colors.length > seriesIndex) {
            //         return colors[0];
            //     }
            //     return colors[seriesIndex];
            // }]
        },
        tooltip: {
            y: {
                formatter: function (val) {
                return "Rp. " + val 
                }
            }
        }
    };
}
