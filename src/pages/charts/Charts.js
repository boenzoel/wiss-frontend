import React, { useState, useEffect  } from "react";
import { Button, Grid } from "@material-ui/core";
import { useTheme } from "@material-ui/styles";
import axios from 'axios';
import {
  CartesianGrid,
  Legend,
  Line,
  LineChart, 
  Pie,
  PieChart,
  ResponsiveContainer,
  Sector,
  Tooltip,
  XAxis,
  YAxis,
  Cell,
  Label,
} from "recharts";

// components
import Widget from "../../components/Widget/Widget";
import ApexLineChart from "./components/ApexLineChart";

//anggota chart
import ApexColumnChartAnggotaRegisterMonthly from "./components/Anggota/ApexColumnChartMonthly";
import ApexColumnChartAnggotaRegisterYearly from "./components/Anggota/ApexColumnChartYearly";

import FusionBarChartAnggotaMonthly from "./components/Anggota/FusionBarChartMonthly"; 
import FusionBarChartAnggotaYearly from "./components/Anggota/FusionBarChartYearly"; 

import FusionPieChartAnggota from "./components/Anggota/FusionPieChart"; 

//simpanan chart
import FusionBarChartSimpananMonthly from "./components/Simpanan/FusionBarChartMonthly"; 
import FusionBarChartSimpananYearly from "./components/Simpanan/FusionBarChartYearly"; 

import FusionPieChartSimpanan from "./components/Simpanan/FusionPieChart"; 

//pinjaman chart
import ApexColumnChartPinjamanMonthly from "./components/Pinjaman/ApexColumnChartMonthly";

import ApexHeatmap from "./components/ApexHeatmap";
import PageTitle from "../../components/PageTitle/PageTitle";

const lineChartData = [
  {
    name: "Page A",
    uv: 4000,
    pv: 2400,
    amt: 2400,
  },
  {
    name: "Page B",
    uv: 3000,
    pv: 1398,
    amt: 2210,
  },
  {
    name: "Page C",
    uv: 2000,
    pv: 9800,
    amt: 2290,
  },
  {
    name: "Page D",
    uv: 2780,
    pv: 3908,
    amt: 2000,
  },
  {
    name: "Page E",
    uv: 1890,
    pv: 4800,
    amt: 2181,
  },
  {
    name: "Page F",
    uv: 2390,
    pv: 3800,
    amt: 2500,
  },
  {
    name: "Page G",
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
];

const pieChartData = [
  { name: "Group A", value: 400 },
  { name: "Group B", value: 300 },
  { name: "Group C", value: 300 },
  { name: "Group D", value: 200 },
  { name: "Group E", value: 200 },
  { name: "Group F", value: 200 },
  { name: "Group G", value: 200 },
];



const data = [
  { name: 'Group A', value: 400 }, { name: 'Group B', value: 300 },
  { name: 'Group C', value: 300 }, { name: 'Group D', value: 200 },
  { name: 'Group E', value: 278 }, { name: 'Group F', value: 189 },
];







export default function Charts(props) {
  var theme = useTheme();

  // local
  var [activeIndex, setActiveIndexId] = useState(0);

  

  var ddAnggota = [
    { name: "Anggota Tetap", value: 400 },
    { name: "Group B", value: 300 },
    { name: "Group C", value: 300 },
    { name: "Group D", value: 200 },
    { name: "Group E", value: 200 },
  ];

  var dataStatus = [
    { name: "Group A", value: 22400 },
    { name: "Group B", value: 300 },
    { name: "Group C", value: 5000 },
    { name: "Group D", value: 22222 },
    { name: "Group E", value: 2222 },
    { name: "Group F", value: 200 },
  ];

  const [dataStatusAnggota, setDataStatusAnggota] = useState(ddAnggota);
  const [dataStatusSimpanan, setDataStatusSimpanan] = useState(dataStatus);
  
  console.log('anggota',dataStatusAnggota)
  console.log('simpanan',dataStatusSimpanan)
  
  const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042', '#ff94f6', '#cbff94'];

  const RADIAN = Math.PI / 180;
  const renderCustomizedLabelStatusAnggota = ({
    cx, cy, midAngle, innerRadius, outerRadius, percent, index, startAngle, endAngle
    }) => {

    const radius = innerRadius + (outerRadius - innerRadius);
    const diffAngle = endAngle - startAngle;
    const delta = ((360-diffAngle)/10)-1;
    const x = cx + (radius + delta) * Math.cos(-midAngle * RADIAN);
    const y = cy + (radius + delta*delta) * Math.sin((-midAngle) * RADIAN);

    return (
      <text x={x} y={y} fill="black" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
        {`${dataStatusAnggota[index].name} = ${dataStatusAnggota[index].value}`} {`( ${(percent * 100).toFixed(2)}% )`}
      </text>
    );
  };

  const renderCustomizedLabelLineStatusAnggota = ({
    cx, cy, midAngle, innerRadius, outerRadius, color, startAngle, endAngle, index
    }) => {
    const RADIAN = Math.PI / 180;
    const diffAngle = endAngle - startAngle;
    const radius = innerRadius + (outerRadius - innerRadius);
    let path='';
    for(let i=0;i<((360-diffAngle)/10);i++){
      path += `${(cx + (radius+i) * Math.cos(-midAngle * RADIAN))},${(cy + (radius+i*i) * Math.sin(-midAngle * RADIAN))} `
    }
    return (
      <polyline points={path} stroke={COLORS[index % COLORS.length]} fill={COLORS[index % COLORS.length]} />
    );
  };
  
  const renderCustomizedLabelStatusSimpanan = ({
    cx, cy, midAngle, innerRadius, outerRadius, percent, index, startAngle, endAngle
    }) => {

    const radius = innerRadius + (outerRadius - innerRadius);
    const diffAngle = endAngle - startAngle;
    const delta = ((360-diffAngle)/22)-1;
    const x = cx + (radius + delta) * Math.cos(-midAngle * RADIAN);
    const y = cy + (radius + delta*delta) * Math.sin((-midAngle) * RADIAN);

    return (
      <text x={x} y={y} fill="black" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
        {`${dataStatusSimpanan[index].name} = ${dataStatusSimpanan[index].value}`} {`( ${(percent * 100).toFixed(2)}% )`}
      </text> 
    );
  }; 

  const renderCustomizedLabelLineStatusSimpanan = ({
    cx, cy, midAngle, innerRadius, outerRadius, color, startAngle, endAngle, index
    }) => {
    const RADIAN = Math.PI / 180;
    const diffAngle = endAngle - startAngle;
    const radius = innerRadius + (outerRadius - innerRadius);
    let path='';
    for(let i=0;i<((360-diffAngle)/22);i++){
      path += `${(cx + (radius+i) * Math.cos(-midAngle * RADIAN))},${(cy + (radius+i*i) * Math.sin(-midAngle * RADIAN))} `
    }
    return (
      <polyline points={path} stroke={COLORS[index % COLORS.length]} fill={COLORS[index % COLORS.length]} />
    );
  };

  function CustomLabel({viewBox, value1, value2, target}){
    const {cx, cy} = viewBox;
    let total = 0;
    
    target.map((target, index) => (
      total += target.value
    ))
  
    return (
     <text x={cx} y={cy} fill="#3d405c" className="recharts-text recharts-label" textAnchor="middle" dominantBaseline="central">
        <tspan alignmentBaseline="middle" fontSize="12">{total}</tspan>
        {/* <tspan fontSize="8">{value2}</tspan> */}
     </text>
    )
  }

  let dataDs = [];
  

  // console.log('first',dataStatusAnggota)
  useEffect(() => {
    async function fetchOnAxios1() {
      await axios.get("http://localhost:8080/dashboard?target=anggota")
      .then(res => {
        let result1 = res.data;
        var anggotaSumm = result1;
        setDataStatusAnggota(anggotaSumm);
        // console.log('anggota',dataStatusAnggota)

      })
      .catch(error => console.log(error.response));
    }

    
    
    fetchOnAxios1();
    
  }, []);


  return (
    <>
      <PageTitle title="Charts Page - Data Display" button={
        <Button
          variant="contained"
          size="medium"
          color="secondary"
        >
          Latest Reports
        </Button>
      } />



      <Grid container spacing={4}>

        <Grid item xs={12} md={6}>
          <Widget title="Data Status Kaanggotaan" upperTitle noBodyPadding>
            <FusionPieChartAnggota />
          </Widget>
        </Grid>

        <Grid item xs={12} md={6}>
          <Widget title="Data Status Simpanan" upperTitle noBodyPadding>
            <FusionPieChartSimpanan />
          </Widget>
        </Grid>

        <Grid item xs={12} md={6}>
          <Widget title="Data Anggota Registrasi Bulanan" upperTitle noBodyPadding>
            <FusionBarChartAnggotaMonthly />
          </Widget>
        </Grid>

        <Grid item xs={12} md={6}>
          <Widget title="Data Anggota Registrasi Tahunan" upperTitle noBodyPadding>
            <FusionBarChartAnggotaYearly />
          </Widget>
        </Grid>

        <Grid item xs={12} md={6}>
          <Widget title="Data Pinjaman Bulanan" upperTitle noBodyPadding>
            <FusionBarChartSimpananMonthly />
          </Widget>
        </Grid>

        <Grid item xs={12} md={6}>
          <Widget title="Data Pinjaman Tahunan" upperTitle noBodyPadding>
            <FusionBarChartSimpananYearly />
          </Widget>
        </Grid>
      </Grid>
    </>
  );
  

}

// ################################################################

function renderActiveShape(props) {
  var RADIAN = Math.PI / 180;
  var {
    cx,
    cy,
    midAngle,
    innerRadius,
    outerRadius,
    startAngle,
    endAngle,
    fill,
    payload,
    percent,
    value,
  } = props;
  var sin = Math.sin(-RADIAN * midAngle);
  var cos = Math.cos(-RADIAN * midAngle);
  var sx = cx + (outerRadius + 10) * cos;
  var sy = cy + (outerRadius + 10) * sin;
  var mx = cx + (outerRadius + 30) * cos;
  var my = cy + (outerRadius + 30) * sin;
  var ex = mx + (cos >= 0 ? 1 : -1) * 22;
  var ey = my;
  var textAnchor = cos >= 0 ? "start" : "end";

  return (
    <g>
      <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>
        {payload.name}
      </text>
      <Sector
        cx={cx}
        cy={cy}
        innerRadius={innerRadius}
        outerRadius={outerRadius}
        startAngle={startAngle}
        endAngle={endAngle}
        fill={fill}
      />
      <Sector
        cx={cx}
        cy={cy}
        startAngle={startAngle}
        endAngle={endAngle}
        innerRadius={outerRadius + 6}
        outerRadius={outerRadius + 10}
        fill={fill}
      />
      <path
        d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`}
        stroke={fill}
        fill="none"
      />
      <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none" />
      <text
        x={ex + (cos >= 0 ? 1 : -1) * 12}
        y={ey}
        textAnchor={textAnchor}
        fill="#333"
      >{`PV ${value}`}</text>
      <text
        x={ex + (cos >= 0 ? 1 : -1) * 12}
        y={ey}
        dy={18}
        textAnchor={textAnchor}
        fill="#999"
      >
        {`(Rate ${(percent * 100).toFixed(2)}%)`}
      </text>
    </g>
  );
}
