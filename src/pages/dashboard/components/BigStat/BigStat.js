import React, { useState } from "react";
import { Grid, Select, MenuItem, Input } from "@material-ui/core";
import { ArrowForward as ArrowForwardIcon } from "@material-ui/icons";
import { useTheme } from "@material-ui/styles";
import { BarChart, Bar } from "recharts";
import classnames from "classnames";

// styles
import useStyles from "./styles";

// components
import Widget from "../../../../components/Widget";
import { Typography } from "../../../../components/Wrappers";



export default function BigStat(props) {
  var { product, total, color, registrations, bounce, data_asset, data_modal, growth, unit_usaha } = props;
  var classes = useStyles();
  var theme = useTheme();



  // local
  var [value, setValue] = useState("2020");

  var [tahun1, setTahun1] = useState("2020");
  var [tahun2, setTahun2] = useState("2019");
  var [unitUsahaSelected, setUnitUsahaSelected] = useState('01');

  var [unitUsahaSel, setUnitUsahaSel] = useState(unit_usaha);

  console.log('unit_usaha',unit_usaha)


  const getUnitUsahaRender = (value, unit_usaha, type, classes) => {
    if(type !== 'unit_usaha'){
      return ('');
    }
    return (
      <Grid item xs={4}>
        <Select
          value={value}
          onChange={e => setUnitUsahaSelected(e.target.value)}
          input={
            <Input
              disableUnderline
              classes={{ input: classes.selectInput }}
            />
          }
          className={classes.select}
          style={{width: '100px'}}
        >
  
          {unit_usaha.map(stat => (
            <MenuItem key={stat.KdKantor} value={stat.KdKantor}>{stat.NamaKantor}</MenuItem>
          ))}
          
        </Select>
      </Grid>
  
    );
  }
  
  

  return (
    <Widget
      header={
        <div className={classes.title}>
          <Typography variant="h5">{product}</Typography>
          

          <Grid item xs={2}>
            <Select
              value={tahun1}
              onChange={e => setValue(e.target.value)}
              input={
                <Input
                  disableUnderline
                  classes={{ input: classes.selectInput }}
                />
              }
              className={classes.select}
            >
              
              <MenuItem value="2020">2020</MenuItem>
              <MenuItem value="2019">2019</MenuItem>
              <MenuItem value="2018">2018</MenuItem>
              <MenuItem value="2017">2017</MenuItem>
            </Select>
          </Grid>

          <Grid item xs={2}>
            <Select
              value={tahun2}
              onChange={e => setValue(e.target.value)}
              input={
                <Input
                  disableUnderline
                  classes={{ input: classes.selectInput }}
                />
              }
              className={classes.select}
            >
              <MenuItem value="2020">2020</MenuItem>
              <MenuItem value="2019">2019</MenuItem>
              <MenuItem value="2018">2018</MenuItem>
              <MenuItem value="2017">2017</MenuItem>
            </Select>
          </Grid>

          
          {getUnitUsahaRender(unitUsahaSelected,unitUsahaSel,'unit_usaha',classes)}
          
         
        </div>
      }
      upperTitle
      bodyClass={classes.bodyWidgetOverflow}
    >
      <div className={classes.totalValueContainer}>
        <div className={classes.totalValue}>
          <Typography size="xxl" color="text" colorBrightness="secondary">
            {convertToRupiah(data_asset[tahun1])}
          </Typography>
          <Typography color={data_asset.percent.profit ? "success" : "secondary"}>
            &nbsp;{data_asset.percent.profit ? "+" : "-"}
            {data_asset.percent.value}%
          </Typography>
        </div>
        <BarChart width={150} height={70} data={getRandomData()}>
          <Bar
            dataKey="value"
            fill={theme.palette[color].main}
            radius={10}
            barSize={10}
          />
        </BarChart>
      </div>
      <div className={classes.bottomStatsContainer}>
        <div className={classnames(classes.statCell, classes.borderRight)}>
          <Grid container alignItems="center">
            <Typography variant="h6">{convertToRupiah(data_modal[tahun1].value)}</Typography>
            <ArrowForwardIcon
              className={classnames(classes.profitArrow, {
                [!data_modal[tahun1].profit]: classes.profitArrowDanger,
              })}
            />
          </Grid>
          <Typography size="sm" color="text" colorBrightness="secondary">
            Data Modal Tahun {tahun1}
          </Typography>
        </div>
        <div className={classes.statCell}>
          <Grid container alignItems="center">
            <Typography variant="h6">{growth.value}%</Typography>
            <ArrowForwardIcon
              className={classnames(classes.profitArrow, {
                [!data_modal[value].profit]: classes.profitArrowDanger,
              })}
            />
          </Grid>
          <Typography size="sm" color="text" colorBrightness="secondary">
            Growth Rate
          </Typography>
        </div>
        <div className={classnames(classes.statCell, classes.borderRight)}>
          <Grid container alignItems="center">
            <Typography variant="h6">
              {convertToRupiah(data_modal[tahun2].value)}
            </Typography>
            <ArrowForwardIcon
              className={classnames(classes.profitArrow, {
                [classes.profitArrowDanger]: !data_modal[tahun1].profit,
              })}
            />
          </Grid>
          <Typography size="sm" color="text" colorBrightness="secondary">
            Data Modal Tahun {tahun2}
          </Typography>
        </div>
      </div>
    </Widget>
  );


  

}

// #######################################################################

function getRandomData() {
  return Array(7)
    .fill()
    .map(() => ({ value: Math.floor(Math.random() * 10) + 1 }));
}



function convertToRupiah(angka)
{
    // var rupiah = '';
    // var angkav = angka.toString().split('.').reverse().join('');
    // var angkarev = angkav.toString().split('').reverse().join('');
    // for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
    // return rupiah.split('',rupiah.length-1).reverse().join('');
    var n = parseFloat(angka);
    // return (parseFloat(angka)).toLocaleString('id-ID', {
    //   style: 'currency',
    //   currency: 'IDR',
    // });

    let simbol = '';
    
    if((angka < 0)) {
      simbol = '-';
      n = n * -1;

      if (n < 1e3) return simbol+""+n;
      else if (n >= 1e3 && n < 1e6) return simbol+""+(n / 1e3).toFixed(1) + " Ribu ";
      else if (n >= 1e6 && n < 1e9) return simbol+""+(n / 1e6).toFixed(1) + " Juta ";
      else if (n >= 1e9 && n < 1e12) return simbol+""+(n / 1e9).toFixed(1) + " Miliar  ";

    }else{
      simbol = '';
    }

    
    
    if (n < 1e3) return simbol+""+n;
    else if (n >= 1e3 && n < 1e6) return simbol+""+(n / 1e3).toFixed(1) + " Ribu ";
    else if (n >= 1e6 && n < 1e9) return simbol+""+(n / 1e6).toFixed(1) + " Juta ";
    else if (n >= 1e9 && n < 1e12) return simbol+""+(n / 1e9).toFixed(1) + " Miliar  ";

      
    return simbol+""+n;
}
