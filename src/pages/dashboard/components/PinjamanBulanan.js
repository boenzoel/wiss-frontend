import React, { useState, useEffect } from "react";
import ApexCharts from "react-apexcharts";
import { useTheme } from "@material-ui/styles";
import axios from 'axios';

var series = [];

var categories = ['Jan','Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Oct','Nov','Des'];

const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042', '#ff94f6', '#cbff94'];

function convertToRupiah(angka)
{
	var rupiah = '';		
	var angkarev = angka.toString().split('').reverse().join('');
	for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
	return rupiah.split('',rupiah.length-1).reverse().join('');
}

export default function ApexColumnChartMonthly() {
    var theme = useTheme();

    var url = 'http://localhost:8080/api/dashboard?target=pinjaman_monthly_summary';

    var [series, setSeries] = useState([])
  
    useEffect(() => {
        axios({
            method: 'POST',
            url: url,
        }).then(function(response,index) {
            let result = response.data;
            
            setSeries(result)
            console.log('pinjaman',result)
      })
    },[]);

    return (
        <ApexCharts
        options={themeOptions(theme,categories,COLORS)}
        series={series}
        type="bar"
        height={350}
        colors={COLORS}
        />
    );
}

// ############################################################
function themeOptions(theme, categories, colors) {
    return {
        plotOptions: {
            bar: {
                horizontal: false,
                columnWidth: '55%',
                endingShape: 'rounded',
            },
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            show: true,
            width: 2,
            colors: ['transparent']
        },
        xaxis: {
            categories: categories,
            ticks: {
                min: -1,
            }
        },
        yaxis: {
            title: {
                text: 'Rp. (Rupiah)'
            },
            ticks: {
                min: -1,
            }
        },
        fill: {
            opacity: 1,
            // colors: ['#FF0000', '#FF00AB', '#00B500'],
            // colors: [function({ value, seriesIndex, w }) {
            //     if(colors.length > seriesIndex) {
            //         return colors[0];
            //     }
            //     return colors[seriesIndex];
            // }]
        },
        tooltip: {
            y: {
                formatter: function (val) {
                return "Rp. " + convertToRupiah(val) 
                }
            }
        }
    };
}
