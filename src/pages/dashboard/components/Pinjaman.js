import React, { useState, useEffect } from "react";
import {
    ResponsiveContainer,
    PieChart,
    Pie,
    Cell,
    Tooltip,
} from "recharts";
import {
    Grid,
} from "@material-ui/core";

import { useTheme } from "@material-ui/styles";
import axios from 'axios';

// styles
import useStyles from "../styles";

import Widget from "../../../components/Widget";
import { Typography } from "../../../components/Wrappers";
import Dot from "../../../components/Sidebar/components/Dot";

let PieChartData = [
    { name: "Group A", value: 400, color: "primary" },
    { name: "Group B", value: 300, color: "secondary" },
    { name: "Group C", value: 300, color: "warning" },
    { name: "Group D", value: 200, color: "success" },
];
  
const colors = ['primary','secondary','warning','success','info','error'];

const COLORS = ['#1976d2', '#dc004e', '#ff9800', '#4caf50','#2196f3', '#f44336'];

const formatCash = n => {
    if (n < 1e3) return n;
    if (n >= 1e3 && n < 1e6) return +(n / 1e3).toFixed(1) + " Ribu ";
    if (n >= 1e6 && n < 1e9) return +(n / 1e6).toFixed(1) + " Jt ";
    if (n >= 1e9 && n < 1e12) return +(n / 1e9).toFixed(1) + " M  ";
    if (n >= 1e12) return +(n / 1e12).toFixed(1) + " T  ";
};

function convertToRupiah(angka)
{
	var rupiah = '';		
	var angkarev = angka.toString().split('').reverse().join('');
	for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
	return rupiah.split('',rupiah.length-1).reverse().join('');
}

const CustomTooltip = ({ active, payload, label }) => {
    if (active) {
        console.log(payload)
      return (
        <div border={1} style={{ backgroundColor: 'white',boxShadow:2 }} className="custom-tooltip">
          <p  className="label">{`NOA : ${payload[0].payload.noa}`}</p>
        </div>
      );
    }
  
    return null;
};


export default function Simjaka() {

    var classes = useStyles();
    var theme = useTheme();
    
    var [simpanan, setSimpanan] = useState([])

    var url = 'http://localhost:8080/dashboard?target=pinjaman'; 
  
    useEffect(() => {
        axios({
            method: 'GET',
            url: url,
        }).then(function(response,index) {

            
            

            let result = response.data;
            // dataSource.data = [];
            let i = 0;
            let datas = [];

            var BreakException= {};
            try {
                result.forEach(element => {
                    // dataSource.data.push( {
                    //     'name' : element.name,
                    //     'value' : element.value,
                    //     'color' : 
                    // } );
                    // simpanan[i].name = element.name;
                    // simpanan[i].name = element.value;

                    if(i > 5){
                        throw BreakException;
                    }

                    // let namaProduk = (element.NamaProdukSimjaka).split(' ');
                    datas.push({ 
                        name: element.namasumberdana == 'KOPERASI' ? 'DANA SENDIRI' : element.namasumberdana, 
                        value: (~~element.totalpinjaman), 
                        noa: element.NOA,
                        color: colors[i] 
                    })

                    
                    
                    i++;
                });
                console.log('datas',datas)
            } catch(e) {
                
                if (e!==BreakException) throw e;
            }
            // datas = [ 
            //     {
            //         name: 'one',
            //         value: 50000, 
            //         color: colors[0] 
            //     },
            //     {
            //         name: 'one',
            //         value: 50000, 
            //         color: colors[0] 
            //     },
            //     {
            //         name: 'one',
            //         value: 50000, 
            //         color: colors[0] 
            //     },
            //     {
            //         name: 'one',
            //         value: 50000, 
            //         color: colors[0] 
            //     },
            //     {
            //         name: 'one',
            //         value: 50000, 
            //         color: colors[0] 
            //     }
            // ];
          setSimpanan(datas)
          console.log('simpanan',simpanan)
      })
    },[]);

    return (
        <Widget title="Pinjaman" upperTitle className={classes.card}>
            <Grid container spacing={2}>
                <Grid item xs={6}>
                    <ResponsiveContainer width="100%" height={144}>
                        <PieChart>
                        <Pie
                            data={simpanan}
                            cx={50}
                            cy={50}
                            innerRadius={30}
                            outerRadius={40}
                            fill="#8884d8"
                            paddingAngle={5}
                            dataKey="value"
                            >
                                {console.log('simpanan',simpanan)}
                            {
                                simpanan.map((entry, index) => <Cell key={`cell-${index}`} fill={theme.palette[entry.color].main} />)
                            }
                            </Pie>
                            <Tooltip content={<CustomTooltip />} />
                        </PieChart>
                    </ResponsiveContainer>
                </Grid>
                <Grid item xs={6}>
                    {/* <div className={classes.pieChartLegendWrapper} style={{ paddingTop:20 }}>
                        {simpanan.map(({ name, value, noa, color }, index) => (
                        <div key={color} className={classes.legendItemContainer} style={{ paddingTop:-53 }}>
                            <div style={{ marginTop:-48 }} ><Dot color={color} style={{ marginTop:5 }}/> </div>
                            <Typography style={{ marginTop: -13, whiteSpace: "nowrap", fontSize: 12 }} >
                            &nbsp;{name}&nbsp;
                            <br></br>
                            <p style={{ marginTop: 5, fontSize: 12 }} >{noa}</p>
                            </Typography>
                            <Typography color="text" colorBrightness="secondary" style={{ marginTop: -48, fontSize: 10 }}>
                            &nbsp;{convertToRupiah(value)}
                            </Typography>
                        </div>
                        ))}
                    </div> */}
                    <div className={classes.pieChartLegendWrapper}>
                        {simpanan.map(({ name, value, color }, index) => (
                        <div key={color} className={classes.legendItemContainer}>
                            <Dot color={color} />
                            <Typography title={name} style={{ whiteSpace: "nowrap", fontSize: 10 }} >
                            &nbsp;{name}&nbsp;
                            </Typography>
                            <Typography color="text" colorBrightness="secondary" style={{ fontSize: 10 }}>
                            &nbsp;{convertToRupiah(value)}
                            </Typography>
                        </div>
                        ))}
                    </div>
                </Grid>
            </Grid>
        </Widget>
    );

}