export default {

    

bigStat: [
    {
      product: "Laporan Keuangan Pusat",
      data_asset: {
        2020: 7637867192.94,
        2019: 7602239922.29,
        percent: { value: 19.14, profit: true }
      },
      color: "primary",
      data_modal: {
        2020: { value: 1462101139.81, profit: true },
        2019: { value: 1284466299.42, profit: true }
        
      },
      growth: { value: 0.46, profit: true },
    }
]

}