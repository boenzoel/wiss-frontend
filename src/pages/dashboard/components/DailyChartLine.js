import React, { useState, useEffect } from "react";
import ReactApexChart from "react-apexcharts";
import { useTheme } from "@material-ui/styles";
import axios from 'axios';


// styles
import useStyles from "../styles";

var lcategories = [1,2,3,4,5,6,7,8,9,10];
var lseries = [
    {
        name: 'X',
        data: [0, 0, 0, 0, 0, 0, 0, 0, 0]
    },
    {
        name: 'Y',
        data: [0, 0, 0, 0, 0, 0, 0, 0, 0]
    },
    {
        name: 'Z',
        data: [0, 0, 0, 0, 0, 0, 0, 0, 0]
    }
];

const COLORS = ['#1976d2', '#dc004e', '#ff9800', '#4caf50','#2196f3', '#f44336', '#03fc7f', '#fc03db', '#cefc03'];
    
export default function DailyChartLine({series,categories}) {
    var theme = useTheme();
    var classes = useStyles();


    // var [series, setSeries] = useState(lseries);
    // var [categories, setCategories] = useState(lcategories);

    console.log('vseries',series)
    // useEffect(() => {
    //     setSeries(vseries)
    //     setCategories(vcategories)
    // },[]);

    return (
    

        // <div id="chart">
        //     <Grid container spacing={2}>
        //         <Grid item xs={10}>
        //             <ResponsiveContainer width="100%" minWidth={500} height={350}>
        //                 <ReactApexChart options={themeOptions(theme,categories)} series={series} type="line" height={350} />
        //             </ResponsiveContainer>
        //         </Grid>
            
        //         <Grid item xs={2}>
        //             <div className={classes.pieChartLegendWrapper}>
        //                 {series.map(({ name }, index) => (
        //                 <div key={index} className={classes.legendItemContainer}>
        //                     <Dot />
        //                     <Typography style={{ whiteSpace: "nowrap", fontSize: 12 }} >
        //                     &nbsp;{name}&nbsp;
        //                     </Typography>
        //                 </div>
        //                 ))}
        //             </div>
        //         </Grid>
        //     </Grid>
        // </div>

        <div id="chart">
            
            <ReactApexChart  options={themeOptions(theme,categories,COLORS)} series={series} type="line" height={350} />
            
        </div>

    );

    
}



function convertToRupiah(angka)
{
	var rupiah = '';		
	var angkarev = angka.toString().split('').reverse().join('');
	for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
	return rupiah.split('',rupiah.length-1).reverse().join('');
}

function option(categories,series) {
    return {
        chart: {
            id:'dailychart',
            height: 350,
            type: 'line',
            zoom: {
                enabled: false
            },
            background: '#fff',
        },
        series: series,
        dataLabels: {
            enabled: false
        },
        stroke: {
            curve: 'smooth',
            width: 2,
        },
        title: {
            text: 'Financing Daily Report',
            align: 'left'
        },
        grid: {
            row: {
              colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
              opacity: 0.5
            },
        },
        xaxis: {
            categories: categories,
        },
        yaxis: {
            range: 10000,
            labels: {
                formatter: function (value) {
                    var rupiah = '';		
                    var angkarev = value.toString().split('').reverse().join('');
                    for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
                    return 'Rp. '+rupiah.split('',rupiah.length-1).reverse().join('');
                }
            },
        },
        markers: {
            size: 4,
            colors: ["#FFA41B"],
            strokeColors: "#fff",
            strokeWidth: 1,
            hover: {
              size: 7,
            }
        },
        legend: {
            formatter: function(seriesName, opts) {
                return seriesName.split(' ') > 1 ? seriesName.split(' ')[0] : seriesName;
            },
            tooltipHoverFormatter: function(seriesName, opts) {
                return seriesName.split(' ') > 1 ? seriesName.split(' ')[1] : seriesName;
            }
        },
        mounted: function () {
            this.uChart()
            console.log('chart1',this.$refs);
        },
        methods: {
            uChart: function() {
                var url = 'http://localhost:8080/api/dashboard?target=real_pinjaman';

                axios({
                    method: 'POST',
                    url: url,
                }).then(function(response,index) {
                    let result = response.data;

                    
                    categories = [];
                    series = [];
                    result['categories'].forEach(element => {
                        categories.push(element)
                    });

                    result['series'].forEach(element => {
                        series.push(element)
                    });

                    console.log('res',result)
                    console.log('cat',categories)
                    console.log('ser',series)

                    
                }).finally(function(){
                    
                });

                this.$refs.chart1.updateSeries(series)

                console.log('chart1',this.$refs);
            }
        }
    }
}

function themeOptions(theme,categories,COLORS) {
    return {
        chart: {
            id:'dailychart',
            height: 350,
            type: 'line',
            zoom: {
                enabled: false
            },
            background: '#fff',
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            curve: 'smooth',
            width: 2,
        },
        title: {
            text: '',
            align: 'left'
        },
        grid: {
            row: {
              colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
              opacity: 0.5
            },
        },
        xaxis: {
            categories: categories,
        },
        yaxis: {
            range: 10000,
            labels: {
                formatter: function (value) {
                    var rupiah = '';
                    if(value === undefined){
                        return '';
                    }		
                    var angkarev = value.toString().split('').reverse().join('');
                    for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
                    return 'Rp. '+rupiah.split('',rupiah.length-1).reverse().join('');
                }
            },
        },
        markers: {
            size: 4,
            colors: ["#FFA41B"],
            strokeColors: "#fff",
            strokeWidth: 1,
            hover: {
              size: 7,
            }
        },
        legend: {
            // formatter: function(seriesName, opts) {
            //     return 'ViV - '+seriesName.split(' - ') > 1 ? seriesName.split(' ')[0] : seriesName;
            // },
            // tooltipHoverFormatter: function(seriesName, opts) {
            //     return seriesName.split(' - ') > 1 ? seriesName.split(' ')[1] : seriesName;
            // },
            // onItemClick: {
            //     toggleDataSeries: true
            // },
            // onItemHover: {
            //     highlightDataSeries: true
            // },
            // markers: {
            //     customHTML: function() {
            //         return '<span class="custom-marker"><i class="fas fa-chart-pie"></i></span>'
            //     },
            // },
        }
    }
}
