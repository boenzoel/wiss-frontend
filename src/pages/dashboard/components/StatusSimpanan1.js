

import React, { Component, useState }  from 'react';
import FusionCharts from "fusioncharts";
import ReactApexChart from "react-apexcharts";
import charts from "fusioncharts/fusioncharts.charts";
import ReactFusioncharts from "react-fusioncharts";
import axios from 'axios';

var dataSource = {
    series: ['100'],
    
    options: {
      chart: {
        type: 'donut',
      },
      labels: ['Nombor'],
      responsive: [{
        breakpoint: 480,
        options: {
          chart: {
            width: 50,
            heigth: 50
          },
          legend: {
            position: 'bottom'
          }
        }
      }]
    },
};

export default function StatusSimpanan1() {

    let opt = {
        chart: {
            type: 'donut',
        },
        responsive: [{
            breakpoint: 480,
            options: {
                chart: {
                    width: 50
                },
                legend: {
                position: 'bottom'
                }
            }
        }],
    }

    const [series, setSeries] = useState([]);
    const [labels, setLabels] = useState([]);
    const [options, setOptions] = useState(opt);

    var url = 'http://localhost:8080/api/dashboard?target=simpanan';

    axios({
        method: 'POST',
        url: url,
    }).then(function(response,index) {
        let result = response.data;
        
        // dataSource.series = [];
        // dataSource.labels = [];

        var serie = dataSource.series;
        var label = dataSource.options.labels;
        
        serie = [];
        label
        result.forEach(element => {
            // console.log('adas',dataSource.series)
            serie.push(element.NamaProduk);
            label.push(element.saldosimpanan);

            
            // serie.push(element.saldosimpanan);
            // label.push(element.NamaProduk);
        });

        // setSeries(serie)
        // setLabels(label)
          
    })

    return (
        <div id="chart">
            <ReactApexChart options={dataSource.options} series={dataSource.series} type="donut" />
        </div>
    );

}