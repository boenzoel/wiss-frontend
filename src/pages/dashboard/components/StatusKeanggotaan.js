import React, { useState, useEffect } from "react";
import {
    ResponsiveContainer,
    PieChart,
    Pie,
    Cell,
} from "recharts";
import {
    Grid,
} from "@material-ui/core";
import { useTheme } from "@material-ui/styles";
import axios from 'axios';

// styles
import useStyles from "../styles";

import Widget from "../../../components/Widget";
import { Typography } from "../../../components/Wrappers";
import Dot from "../../../components/Sidebar/components/Dot";

let PieChartData = [
    { name: "Group A", value: 400, color: "primary" },
    { name: "Group B", value: 300, color: "secondary" },
    { name: "Group C", value: 300, color: "warning" },
    { name: "Group D", value: 200, color: "success" },
];
  
let colors = ['primary','secondary','warning','success','primary'];


function convertToRupiah(angka)
{
    var rupiah = '';		
    var angkarev = angka.toString().split('').reverse().join('');
    for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
    return rupiah.split('',rupiah.length-1).reverse().join('');
}


export default function StatusKeanggotaan() {

    var classes = useStyles();
    var theme = useTheme();
    
    var [statusKeanggotaan, setStatusKeanggotaan] = useState([])

    var url = 'http://localhost:8080/api/dashboard?target=anggota'; 
  
    useEffect(() => {
      axios({
        method: 'POST',
        url: url,
      }).then(function(response,index) {
          let result = response.data;
          // dataSource.data = [];
          let i = 0;
          let datas = [];
          result.forEach(element => {
              // dataSource.data.push( {
              //     'name' : element.name,
              //     'value' : element.value,
              //     'color' : 
              // } );
              // statusKeanggotaan[i].name = element.name;
              // statusKeanggotaan[i].name = element.value;
              datas.push({ 
                name: element.name, 
                value: element.value, 
                color: colors[i] 
              })
              
              i++;
          });
          setStatusKeanggotaan(datas)
          console.log('statusKeanggotaan',statusKeanggotaan)
      })
    },[]);

    return (
        <Widget title="Status Keanggotaaan" upperTitle className={classes.card}>
            <Grid container spacing={2}>
                <Grid item xs={6}>
                <ResponsiveContainer width="100%" height={144}>
                <PieChart>
                    <Pie
                    data={statusKeanggotaan}
                    cx={50}
                    cy={50}
                    innerRadius={30}
                    outerRadius={40}
                    dataKey="value"
                    >
                    {statusKeanggotaan.map((entry, index) => (
                        <Cell
                        key={`cell-${index}`}
                        fill={theme.palette[entry.color].main}
                        />
                    ))}
                    </Pie>
                </PieChart>
                </ResponsiveContainer>
                </Grid>
                <Grid item xs={6}>
                <div className={classes.pieChartLegendWrapper}>
                    {statusKeanggotaan.map(({ name, value, color }, index) => (
                    <div key={color} className={classes.legendItemContainer}>
                        <Dot color={color} />
                        <Typography style={{ whiteSpace: "nowrap", fontSize: 12 }} >
                        &nbsp;{name}&nbsp;
                        </Typography>
                        <Typography color="text" colorBrightness="secondary">
                        &nbsp;{convertToRupiah(value)}
                        </Typography>
                    </div>
                    ))}
                </div>
                </Grid>
            </Grid>
        </Widget>
    );

}