import React, { useState, useEffect } from "react";
import {
  Grid,
  Select,
  OutlinedInput,
  MenuItem,
} from "@material-ui/core";
import { useTheme } from "@material-ui/styles";
import { Close as CloseIcon } from "@material-ui/icons";
import {
  ResponsiveContainer,
} from "recharts";

import axios from 'axios';

// styles
import useStyles from "./styles";

// components
import mock from "./mock";
import Widget from "../../components/Widget";
import PageTitle from "../../components/PageTitle";

import Table from "./components/Table/Table";
import BigStat from "./components/BigStat/BigStat";

import PieChartStatusKeanggotaan from "./components/StatusKeanggotaan"; 

import SimpananChart from "./components/Simpanan";

import SimjakaChart from "./components/Simjaka";

import PinjamanChart from "./components/Pinjaman";

import DailyChartLinePinjaman from "./components/DailyChartLine";

import { useUserDispatch, useUserState, signOut } from "../../context/UserContext";

import Notification from "../../components/Notification";
import { ToastContainer, toast } from "react-toastify";
import { Typography, Button } from "../../components/Wrappers/Wrappers";

const positions = [
  toast.POSITION.TOP_LEFT,
  toast.POSITION.TOP_CENTER,
  toast.POSITION.TOP_RIGHT,
  toast.POSITION.BOTTOM_LEFT,
  toast.POSITION.BOTTOM_CENTER,
  toast.POSITION.BOTTOM_RIGHT,
];


//series line chart default
var lcategories = [1,2,3,4,5,6,7,8,9,10];
var lseries = [
    {
        name: 'X',
        data: [0, 0, 0, 0, 0, 0, 0, 0, 0]
    },
    {
        name: 'Y',
        data: [0, 0, 0, 0, 0, 0, 0, 0, 0]
    },
    {
        name: 'Z',
        data: [0, 0, 0, 0, 0, 0, 0, 0, 0]
    }
];

//check token
let token = localStorage.getItem('token_access');
let license_code = localStorage.getItem('license_code');

axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.headers.common['license_code'] = license_code;
axios.defaults.headers.common['Authorization'] = 'Bearer '+token;

let colors = ['primary','secondary','warning','success','primary'];

let localUnitUsaha = [
  {
    "KdKantor": 1,
    "NamaKantor": "PUSAT"
  },
]

export default function Dashboard(props) {
  var classes = useStyles();
  var theme = useTheme();

  var userDispatch = useUserDispatch();
  var userStateContext = useUserState();

  // local
  var [notificationsPosition, setNotificationPosition] = useState(4);
  var [errorToastId, setErrorToastId] = useState(null);

  
  
  // console.log('token1',token)
  console.log('userStateContext',userStateContext)
  // console.log('userDispatch',userDispatch)
  

  if(token === null || token === ''){
    token = userStateContext.token;
    
    if(token === null || token === '') {
      token = userDispatch.token;
    }
    axios.defaults.headers.common['Authorization'] = 'Bearer '+token;
  }

  let newDate = new Date()
  let month = newDate.getMonth() + 1;

  let year = newDate.getFullYear();

  month = month < 10 ? `0${month}`:`${month}`

  // local
  var [firstGetDataAll, setFirstGetDataAll] = useState(true);

  var [dataLaporanKeuanganPusat, setDataLaporanKeuanganPusat] = useState(mock.bigStat);
  var [dataLaporanKeuanganUnitUsaha, setDataLaporanKeuanganUnitUsaha] = useState(mock.bigStat);

  var [lineChartYear, setLineChartYear] = useState(year);
  var [lineChartMonth, setLineChartMonth] = useState(month);

  var [disabledSelectYear, setDisabledSelectYear] = useState(1);
  var [disabledSelectMonth, setDisabledSelectMonth] = useState(1);

  var [seriesLineChart, setSeriesLineChart] = useState(lseries);
  var [categoriesLineChart, setCategoriesLineChart] = useState(lcategories);

  var [unitUsaha, setUnitUsaha] = useState(localUnitUsaha);

  var url1 = 'http://localhost:8080/api/dashboard?target=laporan_keuangan_pusat_summary&tahun1=2020&tahun2=2019';

  var url2 = 'http://localhost:8080/api/dashboard?target=laporan_keuangan_unit_usaha_summary&unit_usaha=02&tahun1=2020&tahun2=2019';

  var url3 = 'http://localhost:8080/api/dashboard?target=unit_usaha';

  var url4 = 'http://localhost:8080/api/dashboard?target=real_pinjaman&month='+month+'&year='+year;


  //get data firsload document
  useEffect(() => {
    if(firstGetDataAll){
      let props_data_all = {
        setDataLaporanKeuanganPusat,setDataLaporanKeuanganUnitUsaha,
        setUnitUsaha,setSeriesLineChart,setCategoriesLineChart,
        setDisabledSelectYear,setDisabledSelectMonth,setFirstGetDataAll,handleNotificationCall,signOut
      }
      getDataAll(props_data_all);
    };
  },[]);
  

  return (
    <>
      <PageTitle title="Dashboard" button={<Button
      variant="contained"
      size="medium"
      color="secondary"
    >
        Latest Reports
    </Button>} />
      <Grid container spacing={4}>

        <Grid item lg={3} md={4} sm={6} xs={12}>
          <PieChartStatusKeanggotaan />
        </Grid>

        <Grid item lg={3} md={4} sm={6} xs={12}>
          <SimpananChart />
        </Grid>

        <Grid item lg={3} md={4} sm={6} xs={12}>
          <SimjakaChart />
        </Grid>

        <Grid item lg={3} md={4} sm={6} xs={12}>
          <PinjamanChart />
        </Grid>

        <Grid item xs={12}>
          <Widget
            bodyClass={classes.mainChartBody} title='Financing Daily Report'
            header={
              <div className={classes.mainChartHeader}>
                {/* <Typography
                  variant="h5"
                  color="text"
                  colorBrightness="secondary"
                >
                  Tahun
                </Typography> */}
                <Grid item xs={2}>
                  <Select
                    value={lineChartYear}
                    disabled={
                      disabledSelectYear === 1
                    }
                    onChange={e => getDataLineChart(e.target.value, lineChartMonth, setLineChartYear, setLineChartMonth, setDisabledSelectYear,
                      setDisabledSelectMonth)}
                    input={
                      <OutlinedInput
                        labelWidth={0}
                        classes={{
                          notchedOutline: classes.mainChartSelectRoot,
                          input: classes.mainChartSelect,
                        }}
                      />
                    }
                    autoWidth
                  >
                    <MenuItem value="2021">2021</MenuItem>
                    <MenuItem value="2020">2020</MenuItem>
                    <MenuItem value="2019">2019</MenuItem>
                    <MenuItem value="2018">2018</MenuItem>
                    <MenuItem value="2017">2017</MenuItem>
                    <MenuItem value="2016">2016</MenuItem>
                    <MenuItem value="2015">2015</MenuItem>
                  </Select>

                  <Select
                    value={lineChartMonth}
                    disabled={
                      disabledSelectMonth === 1
                    }
                    onChange={e => getDataLineChart(lineChartYear, e.target.value, setLineChartYear, setLineChartMonth, setDisabledSelectYear,
                      setDisabledSelectMonth)}
                    input={
                      <OutlinedInput
                        labelWidth={0}
                        classes={{
                          notchedOutline: classes.mainChartSelectRoot,
                          input: classes.mainChartSelect,
                        }}
                      />
                    }
                    style={{width: '150px'}}
                    >
                    <MenuItem value="01">Januari</MenuItem>
                    <MenuItem value="02">Februari</MenuItem>
                    <MenuItem value="03">Maret</MenuItem>
                    <MenuItem value="04">April</MenuItem>
                    <MenuItem value="05">Mei</MenuItem>
                    <MenuItem value="06">Juni</MenuItem>
                    <MenuItem value="07">Juli</MenuItem>
                    <MenuItem value="08">Agustus</MenuItem>
                    <MenuItem value="09">September</MenuItem>
                    <MenuItem value="10">Oktober</MenuItem>
                    <MenuItem value="11">November</MenuItem>
                    <MenuItem value="12">Desember</MenuItem>
                  </Select>
                </Grid>
                
                
              </div>
            }
          >
            <ResponsiveContainer width="100%" minWidth={500} height={350}>
              <DailyChartLinePinjaman series={seriesLineChart} categories={categoriesLineChart} />
            </ResponsiveContainer>
            
          </Widget>
        </Grid>

        {/* <Grid item xs={12}>
          <PinjamanBulananChart />
        </Grid> */}
        
        {/* <Grid item xs={12}>
          <Widget
            bodyClass={classes.mainChartBody}
            header={
              <div className={classes.mainChartHeader}>
                <Typography
                  variant="h5"
                  color="text"
                  colorBrightness="secondary"
                >
                  Daily Line Chart
                </Typography>
                <div className={classes.mainChartHeaderLabels}>
                  <div className={classes.mainChartHeaderLabel}>
                    <Dot color="warning" />
                    <Typography className={classes.mainChartLegentElement}>
                      Tablet
                    </Typography>
                  </div>
                  <div className={classes.mainChartHeaderLabel}>
                    <Dot color="primary" />
                    <Typography className={classes.mainChartLegentElement}>
                      Mobile
                    </Typography>
                  </div>
                  <div className={classes.mainChartHeaderLabel}>
                    <Dot color="secondary" />
                    <Typography className={classes.mainChartLegentElement}>
                      Desktop
                    </Typography>
                  </div>
                </div>
                <Select
                  value={mainChartState}
                  onChange={e => setMainChartState(e.target.value)}
                  input={
                    <OutlinedInput
                      labelWidth={0}
                      classes={{
                        notchedOutline: classes.mainChartSelectRoot,
                        input: classes.mainChartSelect,
                      }}
                    />
                  }
                  autoWidth
                >
                  <MenuItem value="daily">Daily</MenuItem>
                  <MenuItem value="weekly">Weekly</MenuItem>
                  <MenuItem value="monthly">Monthly</MenuItem>
                </Select>
              </div>
            }
          >
            <ResponsiveContainer width="100%" minWidth={500} height={350}>
              <ComposedChart
                margin={{ top: 0, right: -15, left: -15, bottom: 0 }}
                data={mainChartData}
              >
                <YAxis
                  ticks={[0, 2500, 5000, 7500]}
                  tick={{ fill: theme.palette.text.hint + "80", fontSize: 14 }}
                  stroke={theme.palette.text.hint + "80"}
                  tickLine={false}
                />
                <XAxis
                  tickFormatter={i => i + 1}
                  tick={{ fill: theme.palette.text.hint + "80", fontSize: 14 }}
                  stroke={theme.palette.text.hint + "80"}
                  tickLine={false}
                />
                <Area
                  type="natural"
                  dataKey="dana_sendiri"
                  fill={theme.palette.background.light}
                  strokeWidth={0}
                  activeDot={false}
                />
                <Line
                  type="natural"
                  dataKey="desktop"
                  stroke={theme.palette.primary.main}
                  strokeWidth={2}
                  dot={false}
                  activeDot={false}
                />
                <Line
                  type="linear"
                  dataKey="mobile"
                  stroke={theme.palette.warning.main}
                  strokeWidth={2}
                  dot={{
                    stroke: theme.palette.warning.dark,
                    strokeWidth: 2,
                    fill: theme.palette.warning.main,
                  }}
                />
              </ComposedChart>
            </ResponsiveContainer>
          </Widget>
        </Grid> */}

        
        {dataLaporanKeuanganPusat.map(stat => (
          <Grid item md={4} sm={6} xs={12} key={stat.product}>
            <BigStat {...stat} />
          </Grid>
        ))}
        {dataLaporanKeuanganUnitUsaha.map(stat => (
          <Grid item md={4} sm={6} xs={12}  key={stat.product}>
            <BigStat {...stat} />
          </Grid>
        ))}
        {/* <Grid item xs={12}>
          <Widget
            title="Support Requests"
            upperTitle
            noBodyPadding
            bodyClass={classes.tableWidget}
          >
            <Table data={mock.table} />
          </Widget>
        </Grid> */}

        <ToastContainer
          className={classes.toastsContainer}
          closeButton={
            <CloseButton className={classes.notificationCloseButton} />
          }
          closeOnClick={false}
          progressClassName={classes.notificationProgress}
        />
      </Grid>
    </>
  );

  function getDataAll(props){

    let request1 = axios({method: 'POST',url: url1,});

    let request2 = axios({method: 'POST',url: url2,});

    let request3 = axios({method: 'POST',url: url3,});
    
    let request4 = axios({method: 'POST',url: url4,});

    let error_type = '';
    let error_message = '';
    try {
    
      axios.all([request1, request2, request3, request4]).then(axios.spread((data1, data2, data3, data4) => {
          let result1 = data1.data;

          // console.log('dasss1',result1)
          props.setDataLaporanKeuanganPusat(result1['data_laporan']);

          let result2 = data2.data;
          // console.log('dasss2',result2)
          props.setDataLaporanKeuanganUnitUsaha(result2['data_laporan']);
            
          let result3 = data3.data;
          // console.log('dasss3',result3)
          props.setUnitUsaha(result3);   

          //daily chart line
          let result4 = data4.data;
          var vseries = result4['series']
          var vcategories = result4['categories'];

          props.setSeriesLineChart(vseries)
          props.setCategoriesLineChart(vcategories)
          props.setDisabledSelectYear(0)
          props.setDisabledSelectMonth(0)

          props.setFirstGetDataAll(0)

      })).catch((error) => {
        error_type = error.response.data.error_type;
        error_message = error.response.data.error;

      });

    }catch(e){
      props.handleNotificationCall("error", error_message)
      
      if(error_type === 'token_expired'){
        props.signOut(userDispatch, props.history)
      }
    }
  }

  function getDataLineChart(year, month, setLineChartYear, setLineChartMonth, setDisabledSelectYear, setDisabledSelectMonth)
  {
    setLineChartYear(year)
    setLineChartMonth(month)

    setDisabledSelectYear(1)
    setDisabledSelectMonth(1)

    var url = 'http://localhost:8080/api/dashboard?target=real_pinjaman&month='+month+'&year='+year;

    let request = axios({method: 'POST',url: url,});

    axios.all([request]).then(axios.spread((data) => {

        //daily chart line
        let result = data.data;
        var vseries = result['series']
        var vcategories = result['categories'];

        setSeriesLineChart(vseries)
        setCategoriesLineChart(vcategories)

        setDisabledSelectYear(0)
        setDisabledSelectMonth(0)
        

        // console.log('res',result4)
        // console.log('cat',vcategories)
        // console.log('ser',vseries)
    }));
  }

  function getDataLineChartMonth(year, month, setLineChartMonth)
  {
    setLineChartMonth(month)

    var url = 'http://localhost:8080/api/dashboard?target=real_pinjaman&month='+month+'&year='+year;

    let request = axios({method: 'POST',url: url,});

    axios.all([request]).then(axios.spread((data) => {

        //daily chart line
        let result = data.data;
        var vseries = result['series']
        var vcategories = result['categories'];

        setSeriesLineChart(vseries)
        setCategoriesLineChart(vcategories)

        // console.log('res',result4)
        // console.log('cat',vcategories)
        // console.log('ser',vseries)
    }));
  }

  

  // #############################################################
  function sendNotification(componentProps, options) {
    return toast(
      <Notification
        {...componentProps}
        className={classes.notificationComponent}
      />,
      options,
    );
  }

  function retryErrorNotification() {
    var componentProps = {
      type: "message",
      message: "Message was sent successfully!",
      variant: "contained",
      color: "success",
    };
    toast.update(errorToastId, {
      render: <Notification {...componentProps} />,
      type: "success",
    });
    setErrorToastId(null);
  }

  function handleNotificationCall(notificationType, notificationMessage = '') {
    var componentProps;

    if (errorToastId && notificationType === "error") return;

    switch (notificationType) {
      case "info":
        componentProps = {
          type: "feedback",
          message: "New user feedback received",
          variant: "contained",
          color: "primary",
        };
        break;
      case "error_resensd":
        componentProps = {
          type: "message",
          message: "Message was not sent!",
          variant: "contained",
          color: "secondary",
          extraButton: "Resend",
          extraButtonClick: retryErrorNotification,
        };
        break;
      case "error":
        componentProps = {
          type: "message",
          message: notificationMessage,
          variant: "contained",
          color: "secondary",
        };
        break;
      default:
        componentProps = {
          type: "shipped",
          message: "The item was shipped",
          variant: "contained",
          color: "success",
        };
    }

    var toastId = sendNotification(componentProps, {
      type: notificationType,
      position: positions[notificationsPosition],
      progressClassName: classes.progress,
      onClose: notificationType === "error" && (() => setErrorToastId(null)),
      className: classes.notification,
    });

    if (notificationType === "error") setErrorToastId(toastId);
  }

  function changeNotificationPosition(positionId) {
    setNotificationPosition(positionId);
  }

  function CloseButton({ closeToast, className }) {
    return <CloseIcon className={className} onClick={closeToast} />;
  }
  
}



// #######################################################################
function getRandomData(length, min, max, multiplier = 10, maxDiff = 10) {
  var array = new Array(length).fill();
  let lastValue;

  return array.map((item, index) => {
    let randomValue = Math.floor(Math.random() * multiplier + 1);

    while (
      randomValue <= min ||
      randomValue >= max ||
      (lastValue && randomValue - lastValue > maxDiff)
    ) {
      randomValue = Math.floor(Math.random() * multiplier + 1);
    }

    lastValue = randomValue;

    return { value: randomValue };
  });
}


function getMainChartData() {
  var resultArray = [];
  // var dana_sendiri1 = getDataChart(function(response){
  //   console.log('dana_sendiri',dana_sendiri1)
  // });
  var dana_sendiri = getRandomData(31, 1500, 7500, 7500, 1500);
  var desktop = getRandomData(31, 1500, 7500, 7500, 1500);
  var mobile = getRandomData(31, 1500, 7500, 7500, 1500);

  
  
  console.log('desktop',desktop)

  // axios({
  //     method: 'POST',
  //     url: url,
  // }).then(function(response,index) {
  //     let result = response.data;

  //     let datas = result.data;
  //     for(let i = 0;i < datas[100].length;i++){
  //       dana_sendiri.push({
  //         'value' : datas[100][i],
  //       })
  //     }

  //     for (let i = 0; i < dana_sendiri.length; i++) {
  //       resultArray.push({
  //         dana_sendiri: dana_sendiri[i].value,
  //         desktop: desktop[i].value,
  //         mobile: mobile[i].value,
  //       });
        
  //     }
  //     console.log('resultArray',resultArray)

  // })
  
  for (let i = 0; i < dana_sendiri.length; i++) {
    resultArray.push({
      dana_sendiri: dana_sendiri[i].value,
      desktop: desktop[i].value,
      mobile: mobile[i].value,
    });
  }

  return resultArray;
}
