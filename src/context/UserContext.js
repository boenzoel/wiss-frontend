import React from "react";

import axios from 'axios';

var UserStateContext = React.createContext();
var UserDispatchContext = React.createContext();

function userReducer(state, action) {
  switch (action.type) {
    case "LOGIN_SUCCESS":
      return { ...state, isAuthenticated: true };
    case "SIGN_OUT_SUCCESS":
      return { ...state, isAuthenticated: false };
    case "LOGIN_FAILURE":
        return { ...state, isAuthenticated: false };
    default: {
      throw new Error(`Unhandled action type: ${action.type}`);
    }
  }
}

function UserProvider({ children }) {
  var [state, dispatch] = React.useReducer(userReducer, {
    isAuthenticated: !!localStorage.getItem("token_access"),
    token: localStorage.getItem('token_access'),
  });

  return (
    <UserStateContext.Provider value={state}>
      <UserDispatchContext.Provider value={dispatch}>
        {children}
      </UserDispatchContext.Provider>
    </UserStateContext.Provider>
  );
}

function useUserState() {
  var context = React.useContext(UserStateContext);
  if (context === undefined) {
    throw new Error("useUserState must be used within a UserProvider");
  }
  return context;
}

function useUserDispatch() {
  var context = React.useContext(UserDispatchContext);
  if (context === undefined) {
    throw new Error("useUserDispatch must be used within a UserProvider");
  }
  return context;
}

export { UserProvider, useUserState, useUserDispatch, loginUser, signOut };

// ###########################################################

function loginUser(dispatch, licenseCode, login, password, history, setIsLoading, setError, handleNotificationCall) {
  setError(false);
  setIsLoading(true);

  axios({
    method: 'POST',
    url: 'http://localhost:8080/api/auth/login',
    data: {
      license_code : licenseCode,
      username    : login,
      password : password
    }
  }).then( user => {
      let token = user.data.token;
      let name = user.data.name;
      let koperasi = user.data.koperasi;

      console.log('token',token)
      setTimeout(() => {
        
        localStorage.setItem('token_access', token)
        localStorage.setItem('name', name)
        localStorage.setItem('license_code', licenseCode)
        localStorage.setItem('koperasi', koperasi)
        
        console.log('token_access',localStorage)
        setError(null)
        handleNotificationCall('success', 'Login Successfully.')
        setIsLoading(false)
        dispatch({ type: 'LOGIN_SUCCESS', token: token })

        // localStorage.removeItem("auth_user");

        history.push('/app/dashboard')
      }, 2000);

  }).catch((error) => {
      // ? Show to user that request is failed
      dispatch({ type: "LOGIN_FAILURE" });
      setError(true);
      // console.log('error_call',error.response.data.error)
      handleNotificationCall('error', error.response.data.error)
      setIsLoading(false);
  });

  // if (!!login && !!password) {
  //   setTimeout(() => {
  //     localStorage.setItem('id_token', 1)
  //     setError(null)
  //     setIsLoading(false)
  //     dispatch({ type: 'LOGIN_SUCCESS' })

  //     history.push('/app/dashboard')
  //   }, 2000);
  // } else {
  //   dispatch({ type: "LOGIN_FAILURE" });`z
  //   setError(true);
  //   setIsLoading(false);
  // }
}

function signOut(dispatch, history) {
  localStorage.removeItem("token_access");
  dispatch({ type: "SIGN_OUT_SUCCESS" });
  history.push("/login");
}


