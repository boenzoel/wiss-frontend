import React, { useState, useEffect } from "react";
import { Drawer, IconButton, List } from "@material-ui/core";
import {
  Home as HomeIcon,
  NotificationsNone as NotificationsIcon,
  FormatSize as TypographyIcon,
  FilterNone as UIElementsIcon,
  BorderAll as TableIcon,
  QuestionAnswer as SupportIcon,
  LibraryBooks as LibraryIcon,
  HelpOutline as FAQIcon,
  ArrowBack as ArrowBackIcon,
  PersonPin as UserIcon,
  SaveAlt as SaveIcon,
  ViewList as ReportDataIcon,
  AccountBalance as SimpananIcon,
  Repeat as AutoIcon,
  Book as LaporanIcon,
  Money as PinjamanIcon,
  Update as ProsesIcon,
  Work as CollectionIcon,
  CalendarToday as SimjakaIcon,
  CloudUpload as UploadIcon,
  LaptopWindows as SimIcon,
  Cancel as CancelIcon,
  Edit as EditIcon,
  Replay as BackIcon,
} from "@material-ui/icons";
import { useTheme } from "@material-ui/styles";
import { withRouter } from "react-router-dom";
import classNames from "classnames";

// styles
import useStyles from "./styles";

// components
import SidebarLink from "./components/SidebarLink/SidebarLink";
import Dot from "./components/Dot";

// context
import {
  useLayoutState,
  useLayoutDispatch,
  toggleSidebar,
} from "../../context/LayoutContext";

const structure = [
  { id: 0, label: "Dashboard", link: "/app/dashboard", icon: <HomeIcon /> },
  {
    id: 1,
    label: 'Data Anggota',
    link: '/app/ui',
    icon: <UserIcon />,
    children: [
      { label: 'Input Data', link: '/app/ui/anggota/input', icon: <SaveIcon color="info" />, children: null },
      { label: 'Laporan Data', link: '/app/ui/anggota/laporan', icon: <ReportDataIcon color="success" />, children: null },
    ],
  },
  {
    id: 2,
    label: 'Data Simpanan',
    link: '/app/ui',
    icon: <SimpananIcon />,
    children: [
      { label: 'Input Data', link: '/app/ui/anggota/input', icon: <SaveIcon />, },
      { label: 'Transaksi Data', link: '/app/ui/anggota/laporan', icon: <ProsesIcon />, },
      { 
        label: 'Auto Debet', 
        link: '/app/ui/anggota/laporan',
        icon: <ProsesIcon />,
        children: [
          { label: 'Upload Simpanan', link: '/app/ui/anggota/input', icon: <UploadIcon />, },
          { label: 'Upload Tagihan', link: '/app/ui/anggota/laporan', icon: <UploadIcon />, },
        ],
      },
      { 
        label: 'Laporan Simpanan', 
        link: '/app/ui/anggota/laporan',
        icon: <LaporanIcon />,
        children: [
          { label: 'Laporan Mutasi', link: '/app/ui/anggota/input', icon: <LaporanIcon />, },
          { label: 'Laporan Nominatif', link: '/app/ui/anggota/laporan', icon: <LaporanIcon />, },
        ],
      },
    ],
  },
  {
    id: 3,
    label: 'Data Simpanan Berjangka',
    link: '/app/ui',
    icon: <SimjakaIcon />,
    children: [
      { label: 'Input Data', link: '/app/ui/anggota/input', icon: <SaveIcon />, },
      { label: 'Transaksi Data', link: '/app/ui/anggota/laporan', icon: <ProsesIcon />, },
      { label: 'Laporan Data', link: '/app/ui/anggota/laporan', icon: <LaporanIcon />, },
      { label: 'Proses Data', link: '/app/ui/anggota/laporan', icon: <ProsesIcon />, },
    ],
  },
  {
    id: 4,
    label: 'Data Pinjaman',
    link: '/app/ui',
    icon: <PinjamanIcon />,
    children: [
      { label: 'Simulasi', link: '/app/ui/anggota/input', icon: <UIElementsIcon />, },
      { label: 'Input Data', link: '/app/ui/anggota/laporan', icon: <UIElementsIcon />, },
      { 
        label: 'Transaksi Pinjaman', 
        link: '/app/ui/anggota/laporan',
        icon: <ProsesIcon />,
        children: [
          { label: 'Realisasi', link: '/app/ui/anggota/input', icon: <UIElementsIcon />, },
          { label: 'Angsuran', link: '/app/ui/anggota/laporan', icon: <UIElementsIcon />, },
          { label: 'Pelunasan', link: '/app/ui/anggota/laporan', icon: <UIElementsIcon />, },
          { label: 'Pembatalan Realisasi', link: '/app/ui/anggota/laporan', icon: <UIElementsIcon />, },
          { label: 'Peambatan Angsuran', link: '/app/ui/anggota/laporan', icon: <UIElementsIcon />, },
        ],
      },
      { label: 'Proses Auto Debet', link: '/app/ui/anggota/laporan', icon: <UIElementsIcon />, },
      { 
        label: 'Laporan Pinjaman', 
        link: '/app/ui/anggota/laporan',
        icon: <LaporanIcon />,
        children: [
          { label: 'Laporan Mutasi', link: '/app/ui/anggota/input', icon: <LaporanIcon />, },
          { label: 'Laporan Nominatif', link: '/app/ui/anggota/laporan', icon: <LaporanIcon />, },
          { label: 'Laporan Realisasi', link: '/app/ui/anggota/laporan', icon: <LaporanIcon />, },
          { label: 'Laporan Perfomance', link: '/app/ui/anggota/laporan', icon: <LaporanIcon />, },
        ],
      },
    ],
  },
  {
    id: 5,
    label: 'Collection',
    link: '/app/ui',
    icon: <CollectionIcon />,
    children: [
      { label: 'Input', link: '/app/ui/anggota/input', icon: <SaveIcon />, },
      { label: 'Laporan', link: '/app/ui/anggota/laporan', icon: <LaporanIcon />, },
      { label: 'Slip Potongan Gaji Anggota', link: '/app/ui/anggota/laporan', icon: <PinjamanIcon />, },
    ],
  },
  {
    id: 6,
    label: 'Akutansi',
    link: '/app/ui',
    icon: <PinjamanIcon />,
    children: [
      { label: 'Transaksi Akutansi', link: '/app/ui/anggota/input', icon: <ProsesIcon />, },
      { label: 'Transaksi Jurnal Eliminasi', link: '/app/ui/anggota/laporan', icon: <ProsesIcon />, },
      { label: 'Pembatalan Jurnal', link: '/app/ui/anggota/laporan', icon: <CancelIcon />, },
      { label: 'Pembatan Jurnal Eliminasi', link: '/app/ui/anggota/laporan', icon: <CancelIcon />, },
      { label: 'Revisi Jurnal', link: '/app/ui/anggota/laporan', icon: <EditIcon />, },
      { 
        label: 'Laporan Akutansi', 
        link: '/app/ui/anggota/laporan',
        icon: <LaporanIcon />,
        children: [
          { label: 'Neraca', link: '/app/ui/anggota/input', icon: <LaporanIcon />, },
          { label: 'Neraca Konsolidasi', link: '/app/ui/anggota/laporan', icon: <laporanIcon />, },
          { label: 'Laba Rugi', link: '/app/ui/anggota/laporan', icon: <LaporanIcon />, },
          { label: 'Laporan Jurnal Transaksi', link: '/app/ui/anggota/laporan', icon: <LaporanIcon />, },
          { label: 'Laporan Buku Besar', link: '/app/ui/anggota/laporan', icon: <LaporanIcon />, },
          { label: 'Laporan Rekonsiliasi Antar Rekening', link: '/app/ui/anggota/laporan', icon: <LaporanIcon />, },
        ],
      },
    ],
  },
  {
    id: 7,
    label: 'Proses Akhir Bulan/Tahun',
    link: '/app/ui',
    icon: <ProsesIcon />,
    children: [
      { label: 'Proses Bunga', link: '/app/ui/anggota/input', icon: <SaveIcon />, },
      { 
        label: 'Proses Accrual Pinjaman', 
        link: '/app/ui/anggota/laporan',
        icon: <ProsesIcon />,
        children: [
          { label: 'PYAD', link: '/app/ui/anggota/input', icon: <ProsesIcon />, },
          { label: 'Pengembalian PYAD', link: '/app/ui/anggota/laporan', icon: <BackIcon />, },
        ],
      },
      { 
        label: 'Proses Accrual Simjaka', 
        link: '/app/ui/anggota/laporan',
        icon: <ProsesIcon />,
        children: [
          { label: 'BYMHD', link: '/app/ui/anggota/input', icon: <ProsesIcon />, },
          { label: 'Pengembalian BYMHD', link: '/app/ui/anggota/laporan', icon: <BackIcon />, },
        ],
      },
      { label: 'EOM', link: '/app/ui/anggota/laporan', icon: <LaporanIcon />, },
      { label: 'EOY', link: '/app/ui/anggota/laporan', icon: <LaporanIcon />, },
      { 
        label: 'SHU', 
        link: '/app/ui/anggota/laporan',
        icon: <LaporanIcon />,
        children: [
          { label: 'Perhitungan SHU', link: '/app/ui/anggota/input', icon: <ProsesIcon />, },
          { label: 'Pembagian SHU', link: '/app/ui/anggota/laporan', icon: <ProsesIcon />, },
          { label: 'Laporan SHU', link: '/app/ui/anggota/laporan', icon: <LaporanIcon />, },
        ],
      },
    ],
  },
  // {
  //   id: 1,
  //   label: "Typography",
  //   link: "/app/typography",
  //   icon: <TypographyIcon />,
  // },
  // { id: 2, label: "Tables", link: "/app/tables", icon: <TableIcon /> },
  // {
  //   id: 3,
  //   label: "Notifications",
  //   link: "/app/notifications",
  //   icon: <NotificationsIcon />,
  // },
  // {
  //   id: 4,
  //   label: "UI Elements",
  //   link: "/app/ui",
  //   icon: <UIElementsIcon />,
  //   children: [
  //     { label: "Icons", link: "/app/ui/icons" },
  //     { label: "Charts", link: "/app/ui/charts" },
  //     { label: "Maps", link: "/app/ui/maps" },
  //   ],
  // },
  // { id: 5, type: "divider" },
  // { id: 6, type: "title", label: "HELP" },
  // { id: 7, label: "Library", link: "", icon: <LibraryIcon /> },
  // { id: 8, label: "Support", link: "", icon: <SupportIcon /> },
  // { id: 9, label: "FAQ", link: "", icon: <FAQIcon /> },
  // { id: 10, type: "divider" },
  // { id: 11, type: "title", label: "PROJECTS" },
  // {
  //   id: 12,
  //   label: "My recent",
  //   link: "",
  //   icon: <Dot size="small" color="warning" />,
  // },
  // {
  //   id: 13,
  //   label: "Starred",
  //   link: "",
  //   icon: <Dot size="small" color="primary" />,
  // },
  // {
  //   id: 14,
  //   label: "Background",
  //   link: "",
  //   icon: <Dot size="small" color="secondary" />,
  // },
];

function Sidebar({ location }) {
  var classes = useStyles();
  var theme = useTheme();

  // global
  var { isSidebarOpened } = useLayoutState();
  var layoutDispatch = useLayoutDispatch();

  // local
  var [isPermanent, setPermanent] = useState(true);

  useEffect(function() {
    window.addEventListener("resize", handleWindowWidthChange);
    handleWindowWidthChange();
    return function cleanup() {
      window.removeEventListener("resize", handleWindowWidthChange);
    };
  });

  return (
    <Drawer
      variant={isPermanent ? "permanent" : "temporary"}
      className={classNames(classes.drawer, {
        [classes.drawerOpen]: isSidebarOpened,
        [classes.drawerClose]: !isSidebarOpened,
      })}
      classes={{
        paper: classNames({
          [classes.drawerOpen]: isSidebarOpened,
          [classes.drawerClose]: !isSidebarOpened,
        }),
      }}
      open={isSidebarOpened}
    >
      <div className={classes.toolbar} />
      <div className={classes.mobileBackButton}>
        <IconButton onClick={() => toggleSidebar(layoutDispatch)}>
          <ArrowBackIcon
            classes={{
              root: classNames(classes.headerIcon, classes.headerIconCollapse),
            }}
          />
        </IconButton>
      </div>
      <List className={classes.sidebarList}>
        {structure.map(link => (
          <SidebarLink
            key={link.id}
            location={location}
            isSidebarOpened={isSidebarOpened}
            {...link}
          />
        ))}
      </List>
    </Drawer>
  );

  // ##################################################################
  function handleWindowWidthChange() {
    var windowWidth = window.innerWidth;
    var breakpointWidth = theme.breakpoints.values.md;
    var isSmallScreen = windowWidth < breakpointWidth;

    if (isSmallScreen && isPermanent) {
      setPermanent(false);
    } else if (!isSmallScreen && !isPermanent) {
      setPermanent(true);
    }
  }
}

export default withRouter(Sidebar);
